﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Advanced.Windows.Forms
{
	public class TabCancelEventArgs : CancelEventArgs
	{
		public readonly TabPage Tab;
		
		public TabCancelEventArgs(TabPage tab) {
			this.Tab = tab;
		}
	}
}
