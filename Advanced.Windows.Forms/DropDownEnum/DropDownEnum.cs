﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.Remoting;
using System.Windows.Forms;
using System.Windows.Forms.Layout;

namespace Advanced.Windows.Forms
{
	//public class DropDownEnum<T> : ListControl where T : struct, IComparable, IConvertible, IFormattable
	public class DropDownEnum<T> : ComboBox where T : struct, IComparable, IConvertible, IFormattable
	{
		private readonly Array enumValues = Enum.GetValues(typeof(T));
		
		[Browsable(false)]
		public T SelectedEnumValue {
			get {
				try {
					return (T) enumValues.GetValue(this.SelectedIndex);
				} catch(IndexOutOfRangeException) {
					return default(T);
				}
			}
			
			set {
				for(int i=0; i<this.Items.Count; i++) {
					if(value.Equals(this.Items[i])) {
						this.SelectedIndex = i;
						return;
					}
				}
				
				this.SelectedIndex = -1;
			}
		}
		
		public DropDownEnum() {
			if(!typeof(T).IsEnum)	// check not possible at compile time
				throw new InvalidProgramException("T must be an Enum.");
			
			this.DropDownStyle = ComboBoxStyle.DropDownList;
			
			foreach(var value in enumValues)
				this.Items.Add(value);
			
			this.SelectedIndex = 0;
		}
	}
}
