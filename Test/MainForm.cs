﻿using System;
using System.Windows.Forms;

namespace Test
{
	public partial class MainForm : Form
	{
		public MainForm() {
			InitializeComponent();
			
			enumComboBox1.SelectedValueChanged += (sender, e) => {
				label5.Text = enumComboBox1.SelectedEnumValue.ToString();
			};
		}
	}
}
