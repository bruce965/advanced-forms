﻿using System;

namespace Test
{
	public enum TestEnum
	{
		FirstValue,
		
		SimpleTestValue1,
		
		SecondSimpleValue,
		
		AnotherTestValue,
		
		YetAnotherOne,
		
		LastTestValue
	}
}
