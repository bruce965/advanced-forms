﻿/*
 * Created by SharpDevelop.
 * User: Fabio Iotti
 * Date: 12/04/2014
 * Time: 10.01
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace Test
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.draggableTabControl2 = new Advanced.Windows.Forms.AdvancedTabControl();
			this.tabPage3 = new System.Windows.Forms.TabPage();
			this.label3 = new System.Windows.Forms.Label();
			this.tabPage4 = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.draggableTabControl1 = new Advanced.Windows.Forms.AdvancedTabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.label1 = new System.Windows.Forms.Label();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.label2 = new System.Windows.Forms.Label();
			this.regexTextBox1 = new Advanced.Windows.Forms.RegexTextBox.RegexTextBox();
			this.enumComboBox1 = new Advanced.Windows.Forms.DropDownEnum<Test.TestEnum>();
			this.label5 = new System.Windows.Forms.Label();
			this.draggableTabControl2.SuspendLayout();
			this.tabPage3.SuspendLayout();
			this.tabPage4.SuspendLayout();
			this.draggableTabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// draggableTabControl2
			// 
			this.draggableTabControl2.Controls.Add(this.tabPage3);
			this.draggableTabControl2.Controls.Add(this.tabPage4);
			this.draggableTabControl2.Location = new System.Drawing.Point(12, 118);
			this.draggableTabControl2.Name = "draggableTabControl2";
			this.draggableTabControl2.SelectedIndex = 0;
			this.draggableTabControl2.Size = new System.Drawing.Size(200, 100);
			this.draggableTabControl2.TabIndex = 1;
			// 
			// tabPage3
			// 
			this.tabPage3.Controls.Add(this.label3);
			this.tabPage3.Location = new System.Drawing.Point(4, 22);
			this.tabPage3.Name = "tabPage3";
			this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage3.Size = new System.Drawing.Size(192, 74);
			this.tabPage3.TabIndex = 0;
			this.tabPage3.Text = "tabPage3";
			this.tabPage3.UseVisualStyleBackColor = true;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(23, 7);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(100, 23);
			this.label3.TabIndex = 0;
			this.label3.Text = "label3";
			// 
			// tabPage4
			// 
			this.tabPage4.Controls.Add(this.label4);
			this.tabPage4.Location = new System.Drawing.Point(4, 22);
			this.tabPage4.Name = "tabPage4";
			this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage4.Size = new System.Drawing.Size(192, 74);
			this.tabPage4.TabIndex = 1;
			this.tabPage4.Text = "tabPage4";
			this.tabPage4.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(37, 25);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(100, 23);
			this.label4.TabIndex = 0;
			this.label4.Text = "label4";
			// 
			// draggableTabControl1
			// 
			this.draggableTabControl1.Controls.Add(this.tabPage1);
			this.draggableTabControl1.Controls.Add(this.tabPage2);
			this.draggableTabControl1.Location = new System.Drawing.Point(12, 12);
			this.draggableTabControl1.Name = "draggableTabControl1";
			this.draggableTabControl1.SelectedIndex = 0;
			this.draggableTabControl1.Size = new System.Drawing.Size(200, 100);
			this.draggableTabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(192, 74);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "tabPage1";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(6, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "label1";
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.label2);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(192, 74);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "tabPage2";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(37, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 23);
			this.label2.TabIndex = 0;
			this.label2.Text = "label2";
			// 
			// regexTextBox1
			// 
			this.regexTextBox1.Location = new System.Drawing.Point(260, 12);
			this.regexTextBox1.Name = "regexTextBox1";
			this.regexTextBox1.RegexString = "^(\\d+|)$";
			this.regexTextBox1.Size = new System.Drawing.Size(167, 20);
			this.regexTextBox1.TabIndex = 2;
			// 
			// enumComboBox1
			// 
			this.enumComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.enumComboBox1.FormattingEnabled = true;
			this.enumComboBox1.Items.AddRange(new object[] {
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue,
			Test.TestEnum.FirstValue,
			Test.TestEnum.SimpleTestValue1,
			Test.TestEnum.SecondSimpleValue,
			Test.TestEnum.AnotherTestValue,
			Test.TestEnum.YetAnotherOne,
			Test.TestEnum.LastTestValue});
			this.enumComboBox1.Location = new System.Drawing.Point(260, 51);
			this.enumComboBox1.Name = "enumComboBox1";
			this.enumComboBox1.SelectedEnumValue = Test.TestEnum.FirstValue;
			this.enumComboBox1.Size = new System.Drawing.Size(167, 21);
			this.enumComboBox1.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(260, 75);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(167, 15);
			this.label5.TabIndex = 4;
			this.label5.Text = "label5";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(499, 261);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.enumComboBox1);
			this.Controls.Add(this.regexTextBox1);
			this.Controls.Add(this.draggableTabControl2);
			this.Controls.Add(this.draggableTabControl1);
			this.Name = "MainForm";
			this.Text = "MainForm";
			this.draggableTabControl2.ResumeLayout(false);
			this.tabPage3.ResumeLayout(false);
			this.tabPage4.ResumeLayout(false);
			this.draggableTabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		private System.Windows.Forms.Label label5;
		private Advanced.Windows.Forms.DropDownEnum<Test.TestEnum> enumComboBox1;
		private Advanced.Windows.Forms.RegexTextBox.RegexTextBox regexTextBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.TabPage tabPage3;
		private Advanced.Windows.Forms.AdvancedTabControl draggableTabControl2;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.TabPage tabPage1;
		private Advanced.Windows.Forms.AdvancedTabControl draggableTabControl1;
	}
}
